---
title: "Agenda"
date: 2022-10-11T12:22:11-00:00
---

# Próxima acticidade

A próxima actividade do Centro Linux é:

[Workshop de Introdução ao Fluttter](https://centrolinux.pt/post/2023-fevereiro-workshop-flutter/).

[![Cartaz](https://centrolinux.pt/images/2023.02-workshop_introducao_flutter/post.base.svg )](https://centrolinux.pt/post/2023-fevereiro-workshop-flutter/)

## Esta é a lista completa actividades do Centro Linux.

*(Inclui actividades anteriores)*