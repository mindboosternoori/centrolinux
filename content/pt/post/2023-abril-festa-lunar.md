---
#date: 2023-04-29
description: "Festa de lançamento do Ubuntu 23.04 Lunar Lobster"
featured_image: ""
tags: ["2023", "Abril", "Festa", "Release", "ReleaseParty", "Ubuntu", "Linux", "Abril"]
title: "Festa Lunar"
---

[![Cartaz](https://centrolinux.pt/images/2023.04-Festa_Lunar/post.base.png )](https://osm.org/go/b5crq_xVM?layers=N)


# Festa Lunar
Neste mês de Abril, foi publicada a mais recente versão da mais popular, distribuição de GNU/Linux, o Ubuntu. Assim sendo temos motivo para comemorar, e assinalamos este momento disponibilizando a possibilidade de experimentarem a nova versão, e/ou de em conjunto com a nossa comunidade fazer instalação no seu computador.


Venham conhecer e experimentar o novo Ubuntu 23.04 Lunar Lobster e a comunidade comunidade em Portugal em mais um evento do Centro Linux.

Experimenta! Instala! Actualiza!


## Pré-requisitos para a actividade de Install Party:

Estes pré requisitos aplicam-se a quem quer instalar o Ubuntu 23.04 Lunar Lobster, fazer upgrade de uma versão anterior, ou experimentar no seu computador sem instalar.

Por motivos de gestão de espaço, faremos apenas instalações, upgrades e experimentação sem instlaar no computador do visitante/participante, se for um computador portátil.

1. ter uma cópia de segurança dos vossos sistemas e/ou dados
2. trazerem o vosso computador portátil, e uma pen USB com capacidade de 8GB disponível


## Quando?

No **Sábado dia 29 de Abril de 2023 a partir das 11:30** (haverá interrupção para almoço).

### Plano (provisório de actividades):

| Hora  | Descrição                                                                    |
| ----- | ---------------------------------------------------------------------------- |
| 10:00 | Pequeno-almoço linuxeiro *([detalhes aqui](https://centrolinux.pt/pequeno-almoco-linuxeiro/))* |
| 11:00 | Chegada dos voluntários ao Centro Linux                                      |
| 11:05 | Montagem do espaço para as actividades                                       |
| 11:30 | Iníco das activiades                                                         |
| 11:30 | Apresentação do MILL                                                         |
| 12:45 | Apresentação do Centro Linux                                                 |
| 12:15 | Apresentação do Ubuntu                                                       |
| 12:35 | Apresentação da comunidade Ubuntu Portugal                                   |
| 13:00 | Interrupção para o almoço                                                    |
| ----- | ---------------------------------------------------------------------------- |
| 15:00 | Apresentação do Ubuntu 23.04 Lunar Lobster
| 15:30 | Início da Install Party                                                      |
| 17:30 | Fim da install Party                                                         |
| 17:35 | Corridas de SuperTuxKart (ao estilo LAN party)                               |
| 18:45 | Fim das actividades e desmontagem e arrumação do espaço                      |
| 19:00 | Fim das actividades                                                          |
| ----- | ---------------------------------------------------------------------------- |


### Local:

O Workshop será realizado no Centro Linux que se localiza no Markers In Little Lisbon - MILL, que é em Lisboa, muito perto do Campo dos Mártires da Pátria.
Pode consultar [onde estamos](https://centrolinux.pt/ondeestamos/) para morada e informações detalhadas sobre como pode chegar ao MILL de muitas formas diferentes.

### Inscrições

Inscrições abertas na [página do evento no site do MILL](https://mill.pt/agenda/festa-lunar/).

### Código de Conduta

O Centro Linux é organizado e promovido pela Comunidade Ubuntu-pt (sem que esteja limitado a participações e contribuições da mesma), que se rege pelo [Código de Conduta](https://ubuntu.com/community/code-of-conduct) e pela [declaração de diversidade do Ubuntu](https://ubuntu.com/community/diversity), adicionalmente por estar alojado no [Makers in Little Lisbon](https://mill.pt/), pelo qual estamos muito gratos e que por isso queremos garantir a melhor condição e ambiente possível nesse espaço.

